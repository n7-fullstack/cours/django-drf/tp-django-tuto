# TP Django

## Installer le projet

```bash
python3 -m venv env
source env/bin/activate
pip install django
```

## Lancer le serveur

```bash
cd monprojet
./manage.py migrate
./manage.py createsuperuser
./manage.py runserver
```
