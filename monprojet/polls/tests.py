from django.test import TestCase
from .models import Question, Choice


class TestPoll(TestCase):

    def setUp(self):
        self.question1 = Question.objects.create(question_text='On fait une pause ?')

    def test_resolve_home_url(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'On fait une pause ?')

    def test_detail_page(self):
        response = self.client.get('/{0}'.format(self.question1.id))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'On fait une pause ?')


class TestPollAPI(TestCase):

    def setUp(self):
        Question.objects.create(question_text="Quelle boisson ?")
        Question.objects.create(question_text="Ton framework favori ?")

    def test_get_api_root(self):
        response = self.client.get("/api/v0/")
        self.assertContains(response, "Bienvenue dans l'API")

    def test_get_polls(self):
        response = self.client.get("/api/v0/polls/")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Quelle boisson ?")
        self.assertContains(response, "Ton framework favori ?")

    def test_put_question(self):
        ma_question = {
            'question_text': 'Il faut beau ?',
        }
        response = self.client.post(
            '/api/v0/polls/',
            ma_question,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Question.objects.filter(question_text='Il faut beau ?').exists())

    def test_get_question(self):
        """Test API question detail"""
        response = self.client.get("/api/v0/polls/1")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Quelle boisson ?")

    def test_update_question(self):
        """Test API question detail"""
        data_question = {
            'question_text': 'Tea? Coffee?',
        }
        response = self.client.put(
            "/api/v0/polls/1",
            data_question,
            content_type='application/json'
        )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tea? Coffee?")

    def test_delete_question(self):
        response = self.client.delete("/api/v0/polls/1")
        self.assertEqual(response.status_code, 204)
        self.assertFalse(Question.objects.filter(question_text='Quelle boisson ?').exists())


class TestChoiceAPI(TestCase):

    def setUp(self):
        self.question1 = Question.objects.create(question_text="Quelle boisson ?")
        Choice.objects.create(choice_text="Tea", question=self.question1)
        Choice.objects.create(choice_text="Coffee", question=self.question1)

    def test_get_choices(self):
        response = self.client.get("/api/v0/choices/")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tea")
        self.assertContains(response, "Coffee")

    def test_get_choice(self):
        """Test API question detail"""
        response = self.client.get("/api/v0/choices/1")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Tea")
