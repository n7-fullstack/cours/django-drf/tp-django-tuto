from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Question(models.Model):
    """Model for Question"""
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published', null=True, blank=True)
    categories = models.ManyToManyField(Category, related_name="questions")

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    """Model for choice"""
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text
