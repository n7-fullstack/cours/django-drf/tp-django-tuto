from django.contrib import admin
from polls.models import Question, Choice, Category


class QuestionAdmin(admin.ModelAdmin):
    list_display = (
        'question_text',
        'pub_date',
    )
    date_hierarchy = 'pub_date'


class ChoiceAdmin(admin.ModelAdmin):
    list_display = (
        'choice_text',
        'question',
        'votes',
    )

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Category)
