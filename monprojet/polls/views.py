import datetime
import json
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from django.views.generic import ListView, DetailView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView, ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework import mixins

from .models import Question, Choice
from .serializers import QuestionSerializer, ChoiceSerializer


class IndexView(ListView):
    """Classe vue index basée sur ListView"""
    model = Question
    template_name = 'index.html'
    context_object_name = 'latest_question_list'


class QuestionDetailView(DetailView):
    """Vue question detail"""
    model = Question


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    context = {
        'question': question
    }
    return render(
        request,
        'polls/question_detail.html',
        context)

def results(request, question_id):
    response = "Résultats de la question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("Vous votez à la question %s." % question_id)

def api_root_view(request):
    return HttpResponse("Bienvenue dans l'API")


class QuestionAPIListView(ListCreateAPIView):
    """API Polls get and post"""

    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class QuestionAPIDetailView(RetrieveUpdateDestroyAPIView):
    """API Question retrieve, put and delete"""

    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all().order_by("question_text")
    serializer_class = QuestionSerializer


class ChoiceAPIListView(ListCreateAPIView):
    """API Polls get and post"""

    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer


class ChoiceAPIDetailView(RetrieveUpdateDestroyAPIView):
    """API Polls get and post"""

    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer
