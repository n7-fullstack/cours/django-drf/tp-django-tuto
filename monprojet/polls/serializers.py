from rest_framework import serializers
from .models import Question, Choice
from datetime import datetime


class QuestionSerializer(serializers.ModelSerializer):
    """Serializer Question basé sur Model Serializer"""

    choice_set = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='api_choices_detail'
    )

    class Meta:
        model = Question
        fields = ['id', 'question_text', 'pub_date', 'choice_set']


class ChoiceSerializer(serializers.ModelSerializer):
    """Serializer Question basé sur Model Serializer"""

    class Meta:
        model = Choice
        fields = ['choice_text', 'votes', 'question']
